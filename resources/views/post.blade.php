<x-app-layout>
  <x-slot name="slot">
    <div class="w-full bg-cover bg-center" style="height:32rem; background-image: url({{ asset($post->image) }});">
        <div class="flex items-center justify-center h-full w-full bg-gray-900 bg-opacity-50">
            <div class="text-center">
                <h1 class="text-white text-4xl font-semibold uppercase md:text-4xl">{{ $post->title }}</h1>
                <p class="text-white text-xl pt-3">Posted by {{ $post->author() }} {{ $post->created_at->diffForHumans() }}</p>
            </div>
        </div>
    </div>
    <!-- Post Content-->
    <article class="md:p-12 p-4" id="content">
        <div class="flex flex-col max-w-full md:flex-row my-10">
            <div class="md:flex md:w-2/3 md:pr-6 md:pl-6">
                <div style="white-space: pre-wrap;">{!! $post->content !!}</div>
            </div>
            <div class="flex flex-col pb-6 md:py-0 md:w-1/3 md:pl-6 pt-6">
                <h1 class="text-gray-900 text-2xl font-semibold">Tags</h1>
                <p>{{ $post->tags }}</p>

                <h1 class="text-gray-900 text-2xl font-semibold mt-6">Categories</h1>
                <ul>
                    @foreach($categories as $cat)
                    <li>{{ $cat }}</li>
                    @endforeach
                </ul>

                <h1 class="text-gray-900 text-2xl font-semibold mt-6">Claps</h1>
                <p>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5" />
                    </svg>
                </p>
                <p>{{ $post->claps }} claps</p>
            </div>
        </div>
    </article>
  </x-slot>
</x-app-layout>
