<x-app-layout>
  <x-slot name="slot">
    <header class="masthead" style="background-image: url('assets/img/contact-bg.jpg')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="page-heading">
                        <h1>Medium</h1>
                        <span class="subheading">Feel. Write. Share.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="row gx-4 gx-lg-5 justify-content-center py-6">
      <!--<div class="col-md-10 col-lg-8 col-xl-10">
        @forelse($posts as $post)
          <div class="md:mb-5 p-3">
              <a href="/posts/{{ $post->id }}" class="block rounded-lg relative p-5 transform transition-all duration-300 scale-100 hover:scale-95" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url({{ asset($post->image) }}) center; background-size: cover;">
                  <div class="h-48"></div>
                  <h2 class="text-white text-2xl font-bold leading-tight mb-3 pr-5">{{ $post->title }}</h2>
                  <div class="flex w-full items-center text-sm text-gray-300 font-medium">
                      <div class="flex-1 flex items-center">
                          <div class="rounded-full w-8 h-8 mr-3" style="background: url(https://randomuser.me/api/portraits/women/74.jpg) center; background-size: cover;"></div>
                          <div>{{ $post->author() }}</div>
                      </div>
                      <div><i class="mdi mdi-thumb-up"></i> {{ $post->created_at->diffForHumans() }}</div>
                  </div>
              </a>
          </div>
        @empty
          <p>You have not written any posts yet. <a class="text-primary" href="{{ route('posts.create') }}">Start writing now</a></p>
        @endforelse
    -->
            <div class="col-md-10 col-lg-8 col-xl-8">
                @forelse($posts as $post)
                <div class="post-preview">
                    <a href="posts/{{ $post->id }}">
                        <h2 class="post-title">{{ $post->title }}</h2>
                    </a>
                    <p class="post-meta">
                        Posted by
                        <a href="#!">{{ $post->author() }}</a>
                        {{ $post->created_at->diffForHumans() }}
                    </p>
                </div>
                <!-- Divider-->
                <hr class="my-4" />
                <!-- Pager-->
                @empty
                  <p>You have not written any posts yet. <a class="text-primary" href="{{ route('posts.create') }}">Start writing now</a></p>
                @endforelse
                {{ $posts->links('vendor.pagination.simple-bootstrap-4') }}
            </div>
        </div>
    </div>
  </x-slot>
</x-app-layout>

<footer class="border-top">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <ul class="list-inline text-center">
                    <li class="list-inline-item">
                        <a href="#!">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#!">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#!">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="small text-center text-muted fst-italic">Copyright &copy; Your Website 2021</div>
            </div>
        </div>
    </div>
</footer>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
</body>