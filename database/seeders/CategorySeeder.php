<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'title' => 'Religion'
        ]); 
        Category::create([
            'title' => 'Science'
        ]); 
        Category::create([
            'title' => 'Programming'
        ]); 
        Category::create([
            'title' => 'Fiction'
        ]); 
    }
}
