<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function __construct()
    {
        // Middleware only applied to these methods
        $this->middleware('auth')->only([
            'create', 'store', 'edit', 'update', 'destroy', 'show'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('write-post', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        // do validations here
        $validated = $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'tags' => 'required|string',
            'category' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png',
        ]);

        DB::transaction(function (){
            // $request will not be available here so you can use the request helper
            // i.e. request()
            $post = Post::create(
                [
                    'title' => request()->title,
                    'image' => 'storage/' . request()->image->store('posts', 'public'),
                    'tags' => request()->tags,
                    'content' => request()->content,
                    'user_id' => auth()->id()
                ]
            );

            foreach(request()->category as $cat) {
                PostCategory::create(
                    [
                        'post_id' => $post->id,
                        'category_id' => $cat
                    ]
                );
            }
        });
        
        return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $categories = PostCategory::join('categories', 'categories.id', 'category_id')->where('post_id', $post->id)->pluck('title');
        return view('post', compact('post', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
