<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'image',
        'tags',
        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function author(){
        return User::find($this->user_id)->name;
    }
}
