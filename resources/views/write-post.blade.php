<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      Write Post
    </h2>
  </x-slot>

  <x-slot name="slot">
    <div class="container px-4 px-lg-5">
      <div class="row gx-4 gx-lg-5 ">
        <div class="col-md-10 col-lg-8 col-xl-7">
          @if (session('status'))
              <div class="alert alert-success p-4">
                  {{ session('status') }}
              </div>
          @endif
          @if ($errors->any())
            <div class="alert alert-danger p-4">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="text-red-500">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data" onsubmit="document.getElementById('content').value = document.getElementById('wysiwyg').contentDocument.body.innerHTML; return document.activeElement.getAttribute('value') == 'Submit'">
            @csrf
            <div class="form-floating p-4">
              <label for="name">Title</label><br>
              <input class="form-control w-full" id="title" name="title" type="text" required="required" value="{{ old('title') }}" />
            </div>
            <div class="form-floating p-4">
              <label for="name">Banner Image</label><br>
              <input class="form-control w-full" id="image" name="image" type="file" required="required" value="{{ old('image') }}" />
            </div>
            <div class="form-floating p-4">
              <label for="name">Category</label><br>
              <!-- notice how i put [] after category to make it an array for multiple select -->
              <select class="form-control w-full" name="category[]" required="required" value="{{ old('category[]') }}" multiple="true">
                @foreach($categories as $cat)
                <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                @endforeach
              </select><br>
              <small>select as many as apply</small>
            </div>
            <!--<div class="form-floating p-4">
              <label for="name">Content</label><br>
              <textarea class="form-control w-full" id="content" name="content" required="required">{{ old('content') }}</textarea>
            </div>-->
            <input type="hidden" name="content" id="content">
            <div class="form-floating p-4">
              <label for="name">Tags</label><br>
              <textarea class="form-control w-full" id="tags" name="tags" required="required">{{ old('tags') }}</textarea><br>
              <small>separate by commas</small>
            </div>

            <div class="w-full rounded-xl bg-white p-5 text-black" x-data="app()" x-init="init($refs.wysiwyg, `{{ old('content') }}`)">
                <label>Content</label>
                <div class="border border-gray-400 overflow-hidden">
                    <div class="w-full flex border-b border-gray-200 text-xl text-gray-600">
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('bold')">
                            <i class="mdi mdi-format-bold"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('italic')">
                            <i class="mdi mdi-format-italic"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 mr-1 hover:text-indigo-500 active:bg-gray-50" @click="format('underline')">
                            <i class="mdi mdi-format-underline"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-l border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('formatBlock','P')">
                            <i class="mdi mdi-format-paragraph"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('formatBlock','H1')">
                            <i class="mdi mdi-format-header-1"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('formatBlock','H2')">
                            <i class="mdi mdi-format-header-2"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 mr-1 hover:text-indigo-500 active:bg-gray-50" @click="format('formatBlock','H3')">
                            <i class="mdi mdi-format-header-3"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-l border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('insertUnorderedList')">
                            <i class="mdi mdi-format-list-bulleted"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 mr-1 hover:text-indigo-500 active:bg-gray-50" @click="format('insertOrderedList')">
                            <i class="mdi mdi-format-list-numbered"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-l border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('justifyLeft')">
                            <i class="mdi mdi-format-align-left"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('justifyCenter')">
                            <i class="mdi mdi-format-align-center"></i>
                        </button>
                        <button class="outline-none focus:outline-none border-r border-gray-200 w-10 h-10 hover:text-indigo-500 active:bg-gray-50" @click="format('justifyRight')">
                            <i class="mdi mdi-format-align-right"></i>
                        </button>
                    </div>
                    <div class="w-full">
                        <iframe x-ref="wysiwyg" class="w-full h-96 overflow-y-auto" id="wysiwyg"></iframe>
                    </div>
                </div>
            </div>
            <div class="form-floating p-4 mb-12">
              <input class="btn btn-primary text-uppercase form-control w-full p-2 bg-black text-white" type="submit" value="Submit">
            </div>
          </form>
        </div>
      </div>
    </div>
  </x-slot>
</x-app-layout>
