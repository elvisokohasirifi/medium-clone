<x-app-layout>
  <x-slot name="slot">
    <div class="row gx-4 gx-lg-5 justify-content-center py-6">
      <div class="col-md-10 col-lg-8 col-xl-10">
        @forelse($posts as $post)
          <div class="md:mb-5 p-3">
              <a href="/posts/{{ $post->id }}" class="block rounded-lg relative p-5 transform transition-all duration-300 scale-100 hover:scale-95" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url({{ asset($post->image) }}) center; background-size: cover;">
                  <div class="h-48"></div>
                  <h2 class="text-white text-2xl font-bold leading-tight mb-3 pr-5">{{ $post->title }}</h2>
                  <div class="flex w-full items-center text-sm text-gray-300 font-medium">
                      <div class="flex-1 flex items-center">
                          <div class="rounded-full w-8 h-8 mr-3" style="background: url(https://randomuser.me/api/portraits/women/74.jpg) center; background-size: cover;"></div>
                          <div>{{ $post->author() }}</div>
                      </div>
                      <div><i class="mdi mdi-clock"></i> {{ $post->created_at->diffForHumans() }}</div>
                  </div>
              </a>
          </div>
        @empty
          <p>You have not written any posts yet. <a class="text-primary" href="{{ route('posts.create') }}">Start writing now</a></p>
        @endforelse
        <!-- Pager-->
        {{ $posts->links('vendor.pagination.simple-bootstrap-4') }}
      </div>
    </div>
  </x-slot>
</x-app-layout>
