@if ($paginator->hasPages())
    <nav>
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage() && $paginator->hasMorePages())
            <div class="d-flex justify-content-end mb-4">
                <a class="page-link btn btn-primary text-uppercase" href="{{ $paginator->nextPageUrl() }}" rel="prev">Older Posts →</a>
            </div>
        @endif

        @if (!$paginator->onFirstPage())
            <div class="d-flex justify-content-start mb-4">
                <a class="page-link btn btn-primary text-uppercase" href="{{ $paginator->previousPageUrl() }}" rel="prev">← Newer Posts</a>
            </div>
        @endif
    </nav>
@endif