<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class DashboardController extends Controller
{
    public function home(){
        $posts = Post::orderBy('created_at', 'desc')->simplePaginate(5);
        return view('index', compact('posts'));
    }

    public function dashboard(){
        $posts = Post::where('user_id', auth()->id())->orderBy('created_at', 'desc')->simplePaginate(5);
        return view('dashboard', compact('posts'));
    }
}
